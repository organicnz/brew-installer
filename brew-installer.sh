#!/bin/bash

# Install Command Line Tools
echo Installing xcode
xcode-select --install
sudo xcodebuild -license accept

# Install Homebrew
echo Installing Homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
(echo; echo 'eval "$(/usr/local/bin/brew shellenv)"') >> /Users/$(whoami)/.zprofile
eval "$(/usr/local/bin/brew shellenv)"
export PATH="/opt/homebrew/bin:$PATH"

# Additional installs
brew update-reset
brew tap homebrew/core
brew tap homebrew/cask-versions
brew tap teamookla/speedtest
brew tap cloudflare/cloudflare

# Update brew
echo updating brew
brew update

# Install Homebrew cask
echo installing cask
brew install --cask

# Update brew
echo Updating homebrew
brew update
brew upgrade --cask --greedy

# Verify the Homebrew Installation
echo Homebrew doctor
brew doctor

# Install brew packages
brew tap hashicorp/tap
brew tap heroku/brew && brew install heroku
brew tap lencx/chatgpt https://github.com/lencx/ChatGPT.git
brew tap homebrew/services
brew tap homebrew/cask-fonts

sudo gem install ffi

brew install bfg
brew install hcxtools
brew install hashcat
brew install aircrack-ng
brew install autossh
brew install openssh
brew install imagemagick
brew install wget
brew install tree
brew install git
brew install htop
brew install python@3.11
brew install node
brew install jq
brew install losslesscut
brew install google-drive-file-stream
brew install wifi-explorer
#brew install opencore-patcher
brew install mplayershell
brew install wireguard-tools
#brew install xcode
brew install flutter
brew install speedtest-cli
brew install speedtest --force
brew install google-cloud
brew install dart-sdk
brew install supabase/tap/supabase
brew install doctl
brew install ansible
brew install hashicorp/tap/packer
brew install terraform
brew install awscurl
brew install nmap
brew install unzip
brew install nginx
brew install openjdk
brew install font-inconsolata
brew install deno
brew install postgresql
brew install yarn
brew install gcalcli
brew install gettext
brew install flyctl
brew install cocoapods
brew install clasp
brew install pidof
brew install k9s
brew install ffmpeg
brew install gofireflyio/aiac/aiac
brew install supabase/tap/supabase
brew install gradle
brew install curl
brew install openapi-generator
brew install cloudflared
brew install cloudflare/cloudflare/cloudflared
brew install vlt
brew install azure-cli
brew install mtr
brew install go        
brew install lolcat
brew install gcalcli
brew install libimobiledevice

brew install --cask evernote
brew install --cask todoist
brew install --cask xmind
brew install --cask rapidapi
brew install --cask windscribe
brew install --cask wine-stable 
brew install --cask amnesia-vpn
brew install --cask adobe-creative-cloud
brew install --cask twingate
brew install --cask brave-browser
brew install --cask evernote
brew install --cask lightshot
brew install --cask skitch
brew install --cask whatsapp
brew install --cask obsidian
brew install --cask elmedia-player
brew install --cask pineapple-wifi
brew install --cask github
brew install --cask anydesk
brew install --cask master-pdf-editor
brew install --cask sejda-pdf
brew install --cask sim-genie
brew install --cask firefox
brew install --cask jamwifi
brew install --cask wireshark
brew install --cask warp
brew install --cask protonmail-bridge
brew install --cask protonvpn
brew install --cask cloudflare-warp
brew install --cask rustdesk
brew install --cask outline-manager
brew install --cask squash
brew install --cask cyberduck
brew install --cask uubyeisoeditor
brew install --cask viber
brew install --cask imazing
brew install --cask checkm8-gui
brew install --cask docker
brew install --cask calendar-360
brew install --cask virtualbox
brew install --cask file-transporter
brew install --cask termius
brew install --cask steam
brew install --cask google-chrome
brew install --cask bitwarden
brew install --cask rar
brew install --cask the-unarchiver
brew install --cask wifi-explorer
brew install --cask wifi-explorer-pro
brew install --cask pingplotter
brew install --cask zoom
brew install --cask airtable
brew install --cask visual-studio-code
brew install --cask raycast
brew install --cask vlc
brew install --cask macx-video-converter-pro
brew install --cask tor-browser
brew install --cask microsoft-edge
brew install --cask geekbench
brew install --cask cinebench
brew install --cask drivedx
brew install --cask arc
brew install --cask ngrok
brew install --cask pgadmin4
brew install --cask oracle-jdk
brew install --cask burp-suite
brew install --cask postman
brew install --cask google-cloud-sdk
brew install --cask chatgpt --no-quarantine
brew install --cask speedify
brew install --cask blackmagic-disk-speed-test
brew install --cask spotify
brew install --cask miro
brew install --cask figma
brew install --cask canva
brew install --cask youtube-studio
brew install --cask cleanmymac
brew install --cask feedly
brew install --cask webtorrent
brew install --cask notion
brew install --cask loom
brew install --cask adobe-xd
brew install --cask balenaetcher
brew install --cask deepl
brew install --cask grammarly
brew install --cask slack
brew install --cask android-studio
brew install --cask iterm2
brew install --cask discord
brew install --cask cloudflare/cloudflare/cf-terraforming
brew install --cask google-drive
brew install --cask vmware-fusion
brew install --cask webull
brew install --cask youtube-to-mp3

# Python packages
curl https://bootstrap.pypa.io/get-pip.py | python3
pip install python-dotenv
pip install tk
python3 -m pip install --upgrade pip
python3 -m pip install shodan mmh3

# Node packages
npm install imgproxy

# Go
go install -v github.com/tomnomnom/httprobe@master